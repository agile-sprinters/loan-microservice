package com.agilesprinters.loan.util;

public enum Status {

	NEW("NEW"),
	APPROVED("APPROVED"),
	IN_PROCESS("IN-PROCESS"),
	DECLINED("DECLINED"),
	CLOSED("CLOSED");

	Status(String string) {
		// TODO Auto-generated constructor stub
	}
	
}
