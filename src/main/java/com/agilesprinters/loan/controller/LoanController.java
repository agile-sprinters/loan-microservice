package com.agilesprinters.loan.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.agilesprinters.loan.model.Loan;
import com.agilesprinters.loan.service.LoanService;

@RestController
@RequestMapping("/api")
public class LoanController {

	@Autowired
	private LoanService loanService;

	/**
	 * 
	 * POST http://<domain>:<port>/api/loans/ { "account_id":<value>, "loan_amount":
	 * <value>, "tenure": <value>, "status":
	 * <NEW|APPROVED|IN-PROCESS|DECLINED|CLOSED> }
	 * 
	 * @param loan
	 * @return
	 */
	@PostMapping(path = "/loans", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Loan> createLoan(@RequestBody Loan loan) {

		Loan loanResponse = loanService.createLoan(loan);
		return new ResponseEntity<>(loanResponse, HttpStatus.CREATED);
	}

	/**
	 * Extra Service to get all loans
	 * 
	 * @return
	 */
	@GetMapping(path = "/loans")
	public ResponseEntity<List<Loan>> getAllLoans() {

		List<Loan> loans = loanService.getAllLoans();
		return new ResponseEntity<>(loans, HttpStatus.OK);
	}

	/**
	 * Extra service to read single loan by loadId
	 * 
	 * @param loanId
	 * @return
	 */
	@GetMapping(path = "/loans/{loanId}")
	public ResponseEntity<Loan> getLoan(@PathVariable int loanId) {

		Optional<Loan> loan = loanService.getLoan(loanId);
		return new ResponseEntity<>(loan.get(), HttpStatus.OK);
	}

	/**
	 * GET http://<domain>:<port>/api/customer/{customerid}/loans [ {
	 * "loan_id":<value>, "loan_amount": <value>, "balance_amount":<value>,
	 * "balance_tenure":<value>, "tenure": <value>, "status":
	 * <NEW|APPROVED|IN-PROCESS|DECLINED|CLOSED> } ]
	 * 
	 * @param loanId
	 * @param customerId
	 * @return
	 */
	@GetMapping(path = "/customer/{customerid}/loans")
	public ResponseEntity<List<Loan>> getLoansByCustomerId(@PathVariable("customerid") int customerId) {

		ResponseEntity<List<Loan>> responseEntity;
		List<Loan> loans = loanService.getLoansByCustomerId(customerId);
		if (!loans.isEmpty()) {
			responseEntity = new ResponseEntity<>(loans, HttpStatus.OK);
		} else {
			responseEntity = new ResponseEntity<>(loans, HttpStatus.NOT_FOUND);
		}
		return responseEntity;
	}

	/**
	 * 
	 * GET http://<domain>:<port>/api/customer/{customerid}/loans/{loanid} {
	 * "loan_id":<value>, "loan_amount": <value>, "balance_amount":<value>,
	 * "balance_tenure":<value>, "tenure": <value>, "status":
	 * <NEW|APPROVED|IN-PROCESS|DECLINED|CLOSED> }
	 * 
	 * @param customerId
	 * @param loanid
	 * @return
	 */
	@GetMapping(path = "/customer/{customerid}/loans/{loanid}")
	public ResponseEntity<Loan> getLoanByCustomerId(@PathVariable("customerid") int customerId,
			@PathVariable("loanid") int loanId) {

		ResponseEntity<Loan> responseEntity;
		Loan loan = loanService.getLoanByCustomerId(customerId, loanId);
		if (loan != null) {
			responseEntity = new ResponseEntity<>(loan, HttpStatus.OK);
		} else {
			responseEntity = new ResponseEntity<>(loan, HttpStatus.NOT_FOUND);
		}
		return responseEntity;

	}

	/**
	 * 
	 * PUT http://<domain>:<port>/api/loans/ { "account_id":<value>, "loan_amount":
	 * <value>, "tenure": <value>, "status":
	 * <NEW|APPROVED|IN-PROCESS|DECLINED|CLOSED> }
	 * 
	 * @param loan
	 * @return
	 */
	@PutMapping(path = "/loans", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Loan> updateLoan(@RequestBody Loan loan) {

		Loan loanResponse = loanService.updateLoanStatus(loan);
		return new ResponseEntity<>(loanResponse, HttpStatus.ACCEPTED);
	}

	/**
	 * PUT http://<domain>:<port>/api/customer/{customerid}/loans/{loanid} {
	 * "loan_id":<value>, "amount": <value> }
	 * 
	 * @param loan
	 * @param customerId
	 * @param loanId
	 * @return
	 */
	@PutMapping(path = "customer/{customerid}/loans/{loanid}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Loan> updateLoan(@RequestBody Loan loan, @PathVariable("customerid") int customerId,
			@PathVariable("loanid") int loanId) {

		Loan loanResponse = loanService.updateLoanDetails(loan, customerId, loanId);
		return new ResponseEntity<>(loanResponse, HttpStatus.ACCEPTED);
	}
}
