package com.agilesprinters.loan.service;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agilesprinters.loan.model.Loan;
import com.agilesprinters.loan.repo.LoanRepository;
import com.agilesprinters.loan.util.Status;

@Service
public class LoanService {

	private static final Logger LOG = Logger.getLogger(LoanService.class.getName());

	@Autowired
	private LoanRepository loanRepository;

	public Loan createLoan(Loan loan) {

		return loanRepository.save(loan);
	}

	public List<Loan> getAllLoans() {
		return (List<Loan>) loanRepository.findAll();
	}

	public Optional<Loan> getLoan(int loanId) {
		return loanRepository.findById(loanId);
	}

	public List<Loan> getLoansByCustomerId(int customerId) {
		List<Loan> loans = loanRepository.findByCustomerId(customerId);
		if (loans.isEmpty()) {
			LOG.info("No loans found for provided customerId: " + customerId);
		}
		return loans;
	}

	public Loan getLoanByCustomerId(int customerId, int loanId) {

		return loanRepository.findByCustomerIdAndLoanId(customerId, loanId);
	}

	public Loan updateLoanStatus(Loan loan) {

		return loanRepository.save(loan);
	}

	public Loan updateLoanDetails(Loan loan, int customerId, int loanId) {

		Loan matchingLoan = loanRepository.findByCustomerIdAndLoanId(customerId, loanId);

		if (Status.APPROVED.equals(matchingLoan.getStatus())) {
			matchingLoan.setBalanceAmount(matchingLoan.getBalanceAmount() - loan.getAmount());
			matchingLoan.setBalanceTenure(matchingLoan.getBalanceTenure() - 1);
			matchingLoan.setAmount(loan.getAmount());
			
			if(matchingLoan.getBalanceAmount() == 0 || matchingLoan.getBalanceTenure() == 0) {
				matchingLoan.setStatus(Status.CLOSED);
				matchingLoan.setBalanceTenure(0);
			}
		}
		return loanRepository.save(matchingLoan);
	}

}
